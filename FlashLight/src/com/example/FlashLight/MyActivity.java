package com.example.FlashLight;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Toast;

public class MyActivity extends Activity {

    private FlashLightModel flashLigthModel;

    /** Кнопка включения/выключения светодиода камеры. */
    private ImageButton switchButton;

    /** Элемент для установки значения частоты 'мерцания' светодиода. */
    private SeekBar frequencyBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        switchButton = (ImageButton) findViewById(R.id.switchButton);
        switchButton.setImageResource(R.mipmap.ic_off);

        frequencyBar = (SeekBar) findViewById(R.id.seekBar);

        // Определение имеется ли на устройстве камера.
        Context context = this;
        PackageManager pm = context.getPackageManager();
        if (!pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
            Toast.makeText(getApplicationContext(), "На Вашем устройстве отсутствует вспышка",
                    Toast.LENGTH_SHORT).show();
            AlertDialog.Builder builder = new AlertDialog.Builder(MyActivity.this);
            builder.setTitle("Важное сообщение!")
                    .setMessage("Ошибка камеры!")
                    .setCancelable(false)
                    .setNegativeButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
            return;
        } else {
            flashLigthModel = new FlashLightModel();
        }

        // Определение события клика кнопки.
        switchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flashLigthModel.getTurnOnStatus()) {
                    flashLigthModel.turnOffFlash();
                    switchButton.setImageResource(R.mipmap.ic_off);
                } else {
                    flashLigthModel.turnOnFlash();
                    switchButton.setImageResource(R.mipmap.ic_on);
                }
            }
        });

        // Определение события изменения значения SeekBar'а.
        frequencyBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                flashLigthModel.setFrequency(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        flashLigthModel.cameraRelease();
    }
}
