package com.example.FlashLight;

import android.hardware.Camera;
import android.util.Log;

/**
 * Функционал для управления светодиодом камеры.
 */
public class FlashLightModel {

    /** Состояние светодиода камеры. */
    boolean isTurnOn;

    private Camera.Parameters params;

    private Camera camera = null;

    private StrobeLight strobeLight;

    private Thread strobeTread;

    /**
     * Инициализация полей класса.
     */
    public FlashLightModel() {
            try {
                camera = Camera.open();
                params = camera.getParameters();
            } catch (Exception e) {
                Log.d("flashLight", e.getMessage());
            }

        strobeLight = new StrobeLight();
        isTurnOn = false;
    }

    /**
     * Получение статуса состояния светодиода.
     *
     * @return состояние светодиода 'включен/выключен'.
     */
    public Boolean getTurnOnStatus() {
        return this.isTurnOn;
    }

    /**
     * Включение светодиода.
     */
    public void turnOnFlash() {
        if (!isTurnOn) {
            if (camera == null || params == null) {
                return;
            }

            if (strobeLight.frequencyValue != 0) {
                startThread();
            } else {
                params = camera.getParameters();
                params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                camera.setParameters(params);
                camera.startPreview();
            }

            isTurnOn = true;
        }
    }

    /**
     * Отключение светодиода.
     */
    public void turnOffFlash() {
        if (isTurnOn) {
            if (camera == null || params == null) {
                return;
            }

            stopThread();
            params = camera.getParameters();
            params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            camera.setParameters(params);
            camera.stopPreview();
            isTurnOn = false;
        }
    }

    /**
     * Освобождение русурсов камеры.
     */
    public void cameraRelease() {
        stopThread();
        if (camera != null) {
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }

    /**
     * Установка значения частоты
     *
     * @param freq - частота
     */
    public void setFrequency(int freq) {
        strobeLight.frequencyValue = freq;
        if (strobeTread == null && isTurnOn && freq != 0) {
            startThread();
        } else if (freq == 0) {
            stopThread();
        }
    }

    /**
     * Инициализация и запуск потока для 'мерцания' светодиода.
     */
    private void startThread() {
        strobeLight.stopRunnable = false;
        strobeTread = new Thread(strobeLight);
        strobeTread.start();
    }

    /**
     * Остановка потока.
     */
    private void stopThread() {
        strobeLight.stopRunnable = true;
        strobeTread = null;
    }

    /**
     * Класс для управления частотой светодиода.
     */
    private class StrobeLight implements Runnable {

        private int frequencyValue = 0;

        private boolean stopRunnable = false;

        private final int ONE_SECOND = 1000;

        @Override
        public void run() {
            Camera.Parameters paramsOn = camera.getParameters();
            Camera.Parameters paramsOff = params;
            paramsOn.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            paramsOff.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);

            try {
                while (!stopRunnable) {
                    camera.setParameters(paramsOn);
                    camera.startPreview();
                    Thread.sleep(ONE_SECOND - frequencyValue*10);
                    camera.setParameters(paramsOff);
                    camera.stopPreview();
                    Thread.sleep(ONE_SECOND - frequencyValue*10);
                }
            } catch (Exception e) {
                Log.d("flashLight", e.getMessage());
            }
        }
    }
}
